package fr.epsi.b3dev1;

import com.github.lalyos.jfiglet.FigletFont;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Hello world!
 */
public class App {
 
	public static void main( String[] args ) throws IOException {
		ResourceBundle bundle = ResourceBundle.getBundle( "application" );
		String title = bundle.getString( "titre" );
		System.out.println( FigletFont.convertOneLine( title ) );
		
		String env = bundle.getString( "environement" );
		System.out.println( "Environnement : " + env );
	}
}
